// Package model provides data layout and repository interface for gathered restriction records
package model

import (
	"time"

	"golang.org/x/net/context"
)

// Level of restriction
type Level string

// Known restriction levels
const (
	LevelReject                   Level = "reject"
	LevelFederatedTimelineRemoval Level = "federated_timeline_removal"
	LevelMediaRemoval             Level = "media_removal"
	LevelMediaNSFW                Level = "media_nsfw"
	LevelQuarantinedInstances     Level = "quarantined_instances"
	LevelFollowersOnly            Level = "followers_only"
	LevelAvatarRemoval            Level = "avatar_removal"
	LevelBannerRemoval            Level = "banner_removal"
	LevelRejectDeletes            Level = "reject_deletes"
)

// Record model
type Record struct {
	FirstSeen time.Time `json:"first_seen,omitempty" db:"first_seen"`
	LastSeen  time.Time `json:"last_seen,omitempty" db:"last_seen"`
	Instance  string    `json:"instance,omitempty" db:"instance"`
	Reason    string    `json:"reason,omitempty" db:"reason"`
	Level     Level     `json:"level,omitempty" db:"level"`
	ID        uint64    `json:"id,omitempty" db:"id"`
}

// Source model
type Source struct {
	Last   time.Time `json:"last,omitempty" db:"last"`
	Source string    `json:"source,omitempty" db:"source"`
	ID     uint64    `json:"id,omitempty" db:"id"`
}

// Repository combined interface
type Repository interface {
	Migrate(ctx context.Context) error
	RepositoryRecords
	RepositorySources
}

// RepositoryRecords interface
type RepositoryRecords interface {
	RecordSave(ctx context.Context, record *Record) error
	RecordListAll(ctx context.Context) ([]*Record, error)
	RecordListByInstance(ctx context.Context, instance string) (records []*Record, err error)
}

// RepositorySources interface
type RepositorySources interface {
	SourceSave(ctx context.Context, source *Source) error
	SourceGet(ctx context.Context, code string) (*Source, error)
}
