// Package postgres provides model repository implementation for postgresql database
package postgres

import (
	"context"
	"errors"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/model"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/model/postgres/migration"
)

type repositoryImpl struct {
	DB *sqlx.DB
	repositoryRecords
	repositorySources
}

func prepareNamed(db *sqlx.DB, s string) (r *sqlx.NamedStmt, err error) {
	r, err = db.PrepareNamed(s)

	if err != nil {
		fmt.Printf("error %v preparing %s\n", err, s)
	}

	return
}

// NewRepository returns new postgresql repository
func NewRepository(db *sqlx.DB) (repos model.Repository, err error) {
	repo := &repositoryImpl{
		DB: db,
	}

	return repo, nil
}

func (repo *repositoryImpl) Migrate(ctx context.Context) error {
	source, err := migration.NewSource()
	if err != nil {
		return err
	}

	driver, err := postgres.WithInstance(repo.DB.DB, &postgres.Config{
		MigrationsTable:       "federation_restrictions_changelog",
		MigrationsTableQuoted: false,
		MultiStatementEnabled: false,
		DatabaseName:          "",
		SchemaName:            "",
		StatementTimeout:      0,
		MultiStatementMaxSize: 0,
	})
	if err != nil {
		return err
	}

	m, err := migrate.NewWithInstance("httpfs", source, "postgres", driver)
	if err != nil {
		return err
	}

	err = m.Up()

	switch {
	case err == nil:
		log.Ctx(ctx).Debug().Msg("Migrated.")
	case errors.Is(err, migrate.ErrNoChange):
		log.Ctx(ctx).Debug().Msg("Migration not required.")
	default:
		return err
	}

	err = repo.prepare()
	if err != nil {
		return err
	}

	return nil
}

func (repo *repositoryImpl) prepare() (err error) {
	repo.repositoryRecords, err = prepareRecords(repo.DB)
	if err != nil {
		return
	}

	repo.repositorySources, err = prepareSources(repo.DB)
	if err != nil {
		return
	}

	return
}
