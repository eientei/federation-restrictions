create table instancelimit_records (
  id        bigserial primary key,
  instance  text not null,
  level     text not null,
  reason    text not null,
  start     timestamp not null,
  last_seen timestamp not null,
  unique (instance, level)
);

create table instancelimit_sources (
  id     bigserial primary key,
  source text not null unique,
  last   timestamp not null
);
