package postgres

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/model"
)

type repositorySources struct {
	db   *sqlx.DB
	save *sqlx.NamedStmt
	get  *sqlx.NamedStmt
}

func prepareSources(db *sqlx.DB) (stmt repositorySources, err error) {
	stmt.db = db

	stmt.save, err = prepareNamed(db, querySourceSave)
	if err != nil {
		return
	}

	stmt.get, err = prepareNamed(db, querySourceGet)
	if err != nil {
		return
	}

	return
}

const querySourceSave = `
insert into federation_restrictions_sources(
  source,
  last
) values (
  :source,
  :last
) on conflict(source) do update set
  last = EXCLUDED.last
returning federation_restrictions_sources.*
`

func (r *repositorySources) SourceSave(
	ctx context.Context,
	source *model.Source,
) (err error) {
	err = r.save.GetContext(ctx, source, source)

	return
}

const querySourceGet = `
select 
  *
from federation_restrictions_sources where
  source = :source
`

func (r *repositorySources) SourceGet(
	ctx context.Context,
	code string,
) (source *model.Source, err error) {
	var m model.Source

	err = r.get.GetContext(ctx, &m, struct {
		Source string `db:"source"`
	}{
		Source: code,
	})
	if err == sql.ErrNoRows {
		err = nil
	}

	if err != nil {
		return
	}

	return &m, nil
}
