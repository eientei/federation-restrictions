package postgres

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/model"
)

type repositoryRecords struct {
	db             *sqlx.DB
	save           *sqlx.NamedStmt
	listAll        *sqlx.NamedStmt
	listByInstance *sqlx.NamedStmt
}

func prepareRecords(db *sqlx.DB) (stmt repositoryRecords, err error) {
	stmt.db = db

	stmt.save, err = prepareNamed(db, queryRecordSave)
	if err != nil {
		return
	}

	stmt.listAll, err = prepareNamed(db, queryRecordListAll)
	if err != nil {
		return
	}

	stmt.listByInstance, err = prepareNamed(db, queryRecordListByInstance)
	if err != nil {
		return
	}

	return
}

const queryRecordSave = `
insert into federation_restrictions_records(
  instance,
  level,
  reason,
  first_seen,
  last_seen
) values (
  :instance,
  :level,
  :reason,
  :first_seen,
  :last_seen
) on conflict(instance, level) do update set
  reason = (
    case
    when EXCLUDED.last_seen > federation_restrictions_records.last_seen then 
      EXCLUDED.reason 
    else 
      federation_restrictions_records.reason 
    end
  ),
  last_seen = greatest(federation_restrictions_records.last_seen, EXCLUDED.last_seen)
returning federation_restrictions_records.*
`

func (r *repositoryRecords) RecordSave(
	ctx context.Context,
	record *model.Record,
) (err error) {
	err = r.save.GetContext(ctx, record, record)

	return
}

const queryRecordListAll = `
select 
  *
from federation_restrictions_records 
`

func (r *repositoryRecords) RecordListAll(
	ctx context.Context,
) (records []*model.Record, err error) {
	err = r.listAll.SelectContext(ctx, &records, struct{}{})

	return
}

const queryRecordListByInstance = `
select 
  *
from federation_restrictions_records where
  instance = :instance 
`

func (r *repositoryRecords) RecordListByInstance(
	ctx context.Context,
	instance string,
) (records []*model.Record, err error) {
	err = r.listByInstance.SelectContext(ctx, &records, struct {
		Instance string `db:"instance"`
	}{
		Instance: instance,
	})

	return
}
