// Package fba provides federation restrictions source backed by FBA
package fba

import (
	"errors"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.eientei.org/eientei/federation-restrictions/internal/gather"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/model"
	"golang.org/x/net/context"
	"golang.org/x/net/html"
)

// ErrNilURL nil URL in config
var ErrNilURL = errors.New("nil URL")

// Config options
type Config struct {
	HTTPClient *http.Client
	URL        *url.URL
}

// New returns new source instance
func New(config *Config) (gather.Source, error) {
	if config == nil || config.URL == nil {
		return nil, ErrNilURL
	}

	if config.HTTPClient == nil {
		config.HTTPClient = http.DefaultClient
	}

	return source{
		Config: *config,
	}, nil
}

type source struct {
	Config
}

func (g source) Gather(ctx context.Context, reftime time.Time) (records []*model.Record, err error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, g.URL.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := g.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Close
	}()

	tokenizer := html.NewTokenizer(resp.Body)

	var tda bool

	var tdidx int

	var href, blocklevel, reason, start, lastseen string

	for tt := tokenizer.Next(); tt != html.ErrorToken; tt = tokenizer.Next() {
		switch tt {
		case html.EndTagToken:
			name, _ := tokenizer.TagName()

			switch string(name) {
			case "tr":
				startt, _ := time.Parse("2006-01-02 15:04", start)

				lastseent, _ := time.Parse("2006-01-02 15:04", lastseen)

				href = strings.TrimPrefix(strings.TrimPrefix(href, "http://"), "https://")

				if startt.After(reftime) || lastseent.After(reftime) {
					records = append(records, &model.Record{
						ID:        0,
						Instance:  href,
						Reason:    reason,
						Level:     model.Level(blocklevel),
						FirstSeen: startt,
						LastSeen:  lastseent,
					})
				}

				tdidx = 0
				href = ""
				reason = ""
				start = ""
				lastseen = ""
			case "td":
				tda = false
			case "div":
				blocklevel = ""
			}
		case html.TextToken:
			switch tdidx {
			case 3:
				reason += strings.TrimSpace(string(tokenizer.Text()))
			case 4:
				start += strings.TrimSpace(string(tokenizer.Text()))
			case 5:
				lastseen += strings.TrimSpace(string(tokenizer.Text()))
			}
		case html.StartTagToken:
			name, hasattr := tokenizer.TagName()

			switch string(name) {
			case "div":
				if !hasattr {
					continue
				}

				var hasblocklevel bool

				var blocklevelcand string

				for k, v, more := tokenizer.TagAttr(); ; k, v, more = tokenizer.TagAttr() {
					switch string(k) {
					case "class":
						if string(v) == "block_level" {
							hasblocklevel = true
						}
					case "id":
						blocklevelcand = string(v)
					}

					if !more {
						break
					}
				}

				if hasblocklevel {
					blocklevel = blocklevelcand
				}

			case "td":
				tdidx++

				tda = true
			case "a":
				if !tda || !hasattr || tdidx > 1 {
					continue
				}

				var hasrel bool

				var hrefcand string

				for k, v, more := tokenizer.TagAttr(); ; k, v, more = tokenizer.TagAttr() {
					switch string(k) {
					case "href":
						hrefcand = string(v)
					case "rel":
						hasrel = true
					}

					if !more {
						break
					}
				}

				if hasrel {
					href = hrefcand
				}
			}
		}
	}

	return
}
