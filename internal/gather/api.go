// Package gather provides gathering sources to collect federation restrictions
package gather

import (
	"time"

	"gitlab.eientei.org/eientei/federation-restrictions/internal/model"
	"golang.org/x/net/context"
)

// Source gatherer source interface
type Source interface {
	Gather(ctx context.Context, start time.Time) (records []*model.Record, err error)
}
