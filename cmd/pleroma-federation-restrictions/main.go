// Package main, binary entrypoint
package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/patrickmn/go-cache"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/envar"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/gather"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/gather/fba"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/model"
	"gitlab.eientei.org/eientei/federation-restrictions/internal/model/postgres"
	"golang.org/x/net/context"
)

var (
	envListen      = envar.ResolveString("LISTEN", ":8080")
	envPostgresURL = envar.ResolveString(
		"POSTGRES_URL",
		"postgres://pleroma:pleroma@127.0.0.1:5432/pleroma?sslmode=disable",
	)
	envGatherPeriod = envar.ResolveDuration("GATHER_PERIOD", time.Hour)
	envCachePeriod  = envar.ResolveDuration("CACHE_PERIOD", time.Minute*10)
)

func backgroundFetchSource(
	ctx context.Context,
	repo model.Repository,
	k string,
	source gather.Source,
) {
	s, err := repo.SourceGet(ctx, k)
	if err != nil {
		log.Error().Err(err).Str("source", k).Msg("getting source last time")

		return
	}

	records, err := source.Gather(ctx, s.Last)
	if err != nil {
		log.Error().Err(err).Str("source", k).Msg("gathering records")

		return
	}

	if len(records) == 0 {
		return
	}

	log.Info().Str("source", k).Int("records", len(records)).Msg("collected")

	for _, r := range records {
		err = repo.RecordSave(ctx, r)
		if err != nil {
			log.Error().Err(err).Str("source", k).Interface("record", *r).Msg("saving record")
		}

		if r.FirstSeen.After(s.Last) {
			s.Last = r.FirstSeen
		}
	}

	s.Source = k

	err = repo.SourceSave(ctx, s)
	if err != nil {
		log.Error().Err(err).Str("source", k).Msg("saving source")
	}
}

func backgroundFetch(
	ctx context.Context,
	repo model.Repository,
	sources map[string]gather.Source,
	period time.Duration,
) {
	for {
		for k, source := range sources {
			backgroundFetchSource(ctx, repo, k, source)
		}

		time.Sleep(period)
	}
}

func handlerListAll(data *cache.Cache, repo model.Repository) http.HandlerFunc {
	var modtime time.Time

	return func(writer http.ResponseWriter, request *http.Request) {
		var err error

		v, ok := data.Get("list")
		if !ok {
			v, err = repo.RecordListAll(request.Context())
			if err != nil {
				http.Error(writer, err.Error(), http.StatusInternalServerError)

				return
			}

			records, _ := v.([]*model.Record)

			var bs []byte

			bs, err = json.Marshal(records)
			if err != nil {
				http.Error(writer, err.Error(), http.StatusInternalServerError)

				return
			}

			data.SetDefault("list", bs)

			modtime = time.Now()

			v = bs
		}

		bs, _ := v.([]byte)

		http.ServeContent(writer, request, "data.json", modtime, bytes.NewReader(bs))
	}
}

func handlerListInstance(data *cache.Cache, repo model.Repository) func(http.ResponseWriter, *http.Request) {
	var modtime time.Time

	return func(writer http.ResponseWriter, request *http.Request) {
		var err error

		instance := strings.TrimPrefix(request.URL.Path, "/api/v1/federation/restrictions/list/")

		key := "list_" + instance

		v, ok := data.Get(key)
		if !ok {
			v, err = repo.RecordListByInstance(request.Context(), instance)
			if errors.Is(err, sql.ErrNoRows) {
				http.NotFound(writer, request)

				return
			}

			if err != nil {
				http.Error(writer, err.Error(), http.StatusInternalServerError)

				return
			}

			var bs []byte

			bs, err = json.Marshal(v)
			if err != nil {
				http.Error(writer, err.Error(), http.StatusInternalServerError)

				return
			}

			data.SetDefault(key, bs)

			v = bs

			modtime = time.Now()
		}

		bs, _ := v.([]byte)

		http.ServeContent(writer, request, instance+".json", modtime, bytes.NewReader(bs))
	}
}

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: time.RFC3339,
	})

	envar.PrintEnv()

	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	db := sqlx.MustOpen("postgres", envPostgresURL)

	postgresRegistry, err := postgres.NewRepository(db)
	if err != nil {
		panic(err)
	}

	err = postgresRegistry.Migrate(ctx)
	if err != nil {
		panic(err)
	}

	sources := make(map[string]gather.Source)

	for _, v := range os.Environ() {
		parts := strings.SplitN(v, "=", 2)

		if !strings.HasPrefix(parts[0], "GATHER_FBA_") || !strings.HasSuffix(parts[0], "_URL") {
			continue
		}

		k := strings.TrimSuffix(strings.TrimPrefix(parts[0], "GATHER_FBA_"), "_URL")

		fbaurl, parseerr := url.Parse(parts[1])
		if parseerr != nil {
			log.Error().Err(parseerr).Str("env", v).Msg("parsing environment variable")

			panic(parseerr)
		}

		fbasource, parseerr := fba.New(&fba.Config{
			URL: fbaurl,
		})
		if parseerr != nil {
			log.Error().Err(parseerr).Str("env", v).Msg("creating fba gatherer")

			panic(parseerr)
		}

		sources[k] = fbasource

		log.Info().Str("key", k).Str("url", parts[1]).Msg("fba gatherer added")
	}

	go backgroundFetch(ctx, postgresRegistry, sources, envGatherPeriod)

	data := cache.New(envCachePeriod, envCachePeriod)

	http.HandleFunc("/api/v1/federation/restrictions/list", handlerListAll(data, postgresRegistry))
	http.HandleFunc("/api/v1/federation/restrictions/list/", handlerListInstance(data, postgresRegistry))

	err = http.ListenAndServe(envListen, nil)
	if err != nil {
		panic(err)
	}
}
