federation-restrictions
===

Gathers list of remote instances defederating local instance.

Provides API to query list of defederations.

Currently only FBA gatherer is supported.

List of gatherers can be configured by providing environment variables in form 

```
GATHER_FBA_<KEY>_URL=<URL>
```

e.g.

```
GATHER_FBA_RYONA_AGENCY_URL=https://fba.ryona.agency/?domain=eientei.org
```

API
---

```
GET /api/v1/federation/restrictions/list
```

Returns full list of defederations


```
GET /api/v1/federation/restrictions/list/some.domain.tld
```

Returns list of defederations from some.domain.tld

Data
----

Sample response

```json
[
  {
    "first_seen": "2023-04-22T09:30:00Z",
    "last_seen": "2023-04-22T09:30:00Z",
    "instance": "social.4netguides.org",
    "reason": "Rule 4",
    "level": "followers_only",
    "id": 544
  }
]
```

FE patches
---
Pleroma FE: https://gitlab.eientei.org/eientei/pleroma-fe/-/commits/federation_restrictions
