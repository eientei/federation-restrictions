module gitlab.eientei.org/eientei/federation-restrictions

go 1.20

require (
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.0 // indirect
	github.com/rs/zerolog v1.15.0
	golang.org/x/net v0.9.0
)

require github.com/patrickmn/go-cache v2.1.0+incompatible

require (
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
)
